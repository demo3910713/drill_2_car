/*
 The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
"Car 33 is a car year goes here car make goes here car model goes here"*/
function carDetailsById(data, ids, callback) {
  // check the data  properly given correct or not
  if (Array.isArray(data) && typeof ids === "number") {
    let car_details = data.filter((value) => {
      return value.id === ids;
    });
    if (car_details.length > 0) {
      return callback(car_details[0]);
    }
  }
}
// format function for output printing
function formatCarDetails(car) {
  return (
    "Car " +
    car.id +
    " is a car year " +
    car.car_year +
    " car make " +
    car.car_make +
    " car model " +
    car.car_model
  );
}

// export the code
module.exports = { carDetailsById, formatCarDetails };
