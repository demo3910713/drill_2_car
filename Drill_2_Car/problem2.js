/* 
 The dealer needs the information on the last car in their inventory.
Execute a function to find what the make and model of the last car in the inventory is?
Log the make and model into the console in the format of:("Last car is a car make goes here car model goes here");
*/
function lastCarInformation(data, callback) {
  // check the data properly given correct or not
  if (Array.isArray(data)) {
    // Findout the last car information
    let last_index = data.filter(
      (Element, index) => index == data.length - 1
    );
    if (last_index.length > 0) {
      return callback(last_index[0]);
    }
  } else {
    return "Data Insufficient ";
  }
}
function formatLastCarDetails(car) {
  return (
    "Last Car is a car make " + car.car_year + " car model " + car.car_model
  );
}
// Export the Code
module.exports = { lastCarInformation, formatLastCarDetails };
