// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
function onlyBNWandAudiInformation(data) {
  // check the data properly given correct or not
  if (Array.isArray(data)) {
    const detailsOfBNWandAudi = data.filter(
      (obj) => (obj.car_make === "BNW") | (obj.car_make == "Audi")
    );
    return JSON.stringify(detailsOfBNWandAudi);
  } else {
    return " Data Insufficient ";
  }
}
// Export the code
module.exports = { onlyBNWandAudiInformation };
