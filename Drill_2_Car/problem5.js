// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function countOfCarsOlderThanTheYear2000(data) {
  // check the data properly given correct or not
  if (Array.isArray(data)) {
    let noOfcars = data.filter((obj) => obj.car_year < 2000);
    let count_of_cars = noOfcars.length;
    return "no of cars manufuring older than the year 2000 :  " + count_of_cars;
  } else {
    return " Data Insufficient ";
  }
}
// Export the Code
module.exports = { countOfCarsOlderThanTheYear2000 };
