// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function allCarYears(data) {
  // check the data properly given correct or not
  if (Array.isArray(data)) {
    // To store the years
    const car_years = data.map((data) => {
      return data.car_year;
    });
    return car_years.toString();
  } else {
    return " Data Insufficient ";
  }
}

// Export the Code
module.exports = { allCarYears };
