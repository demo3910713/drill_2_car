// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
function listInAlphabeticalOrder(data) {
  // check the data properly given correct or not
  if (Array.isArray(data)) {
    // to store the car models
    let carModelNames = data.map((obj) => {
      return obj.car_model;
    });
    return carModelNames.sort().toString();
  } else {
    return " Data Insufficent ";
  }
}
// Export the Code
module.exports = { listInAlphabeticalOrder };
